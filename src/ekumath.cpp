#include <ekumath.h>
using namespace std;

namespace ekumath {

Matrix::Matrix(unsigned int row, unsigned int col, float fill) {
	m_ = vector<vector<float>>(row, vector<float>(col, fill));
}

Matrix::Matrix(std::initializer_list<float> init_list) {
    for(auto rowVector: init_list) {
        m_.push_back(vector<float>(1, rowVector));
    }
}

Matrix::Matrix(initializer_list<initializer_list<float>> init_list) {
    for(auto vec: init_list) {
        if(rows() > 0 && cols() != vec.size()) {
            throw std::runtime_error("Irregular matrix declared.");
        }
        m_.push_back(vector<float>(vec));
    }
}

Matrix Matrix::Identity(unsigned int size) {
    Matrix I(size, size);
    for(auto i = 0; i < I.cols(); i++) {
        I.m_.at(i).at(i) = static_cast<float>(1.);
    }
    return I;
}

Matrix Matrix::Random(unsigned int row, unsigned int col) {
    Matrix randomMatrix;
    for(auto i = 0; i < row; i++) {
        vector<float> row;
        for(auto j = 0; j < col; j++) {
            row.push_back(static_cast<float>(rand() % 100));
        }
        randomMatrix.m_.push_back(row);
    }
    return randomMatrix;
}

bool Matrix::operator==(const Matrix &other) const {
    if(this->cols() != other.cols() || this->rows() != other.rows()) {
        return false;
    }
    auto it = m_.begin();
    auto otherIt = other.m_.begin();
    while(it != m_.end()) {
        if(*it != *otherIt) {
            return false;
        }
        it++;
        otherIt++;
    }
    return true;
}

bool Matrix::operator!=(const Matrix &other) const {
    return !(*this==other);
}

float& Matrix::operator()(unsigned int row, unsigned int col) {
    return m_.at(row).at(col); // It could thrown an out_of_range exception
}

Matrix Matrix::operator+(Matrix &mat) {
    if(cols() != mat.cols() || rows() != mat.rows()) {
        throw runtime_error("Tried to add different size matrix");
    }
    Matrix copy(*this);
    for(unsigned int i = 0; i < rows(); i++) {
        for(unsigned int j = 0; j < cols(); j++) {
            copy(i, j) = (*this)(i, j) + mat(i, j);
        }
    }
    return copy;
}

Matrix Matrix::operator*(Matrix &mat) {
    if(cols() != mat.cols() || rows() != mat.rows()) {
        throw runtime_error("Tried to add different size matrix");
    }
    Matrix copy(*this);
    for(unsigned int i = 0; i < rows(); i++) {
        for(unsigned int j = 0; j < cols(); j++) {
            copy(i, j) = (*this)(i, j) * mat(i, j);
        }
    }
    return copy;
}

Matrix Matrix::operator|(Matrix mat) {
    return this->concatenate(mat);
}

Matrix Matrix::operator^(const int exp) {
    Matrix copy(*this);
    for(unsigned int i = 0; i < copy.rows(); i++) {
        for(unsigned int j = 0; j < copy.cols(); j++) {
            copy(i, j) = pow(copy(i, j), exp);
        }
    }
    return copy;
}

int Matrix::rows() const {
    return m_.size();
}

int Matrix::cols() const {
    if(rows() > 0) {
        return m_[0].size();
    } else {
        return 0;
    }
}

int Matrix::size() const {
    return rows() * cols();
}

Matrix Matrix::transpose() {
    Matrix T(cols(), rows());
    for(unsigned int i = 0; i < rows(); i++) {
        for(unsigned int j = 0; j < cols(); j++) {
            T(i, j) = (*this)(j, i);
        }
    }
    return T;
}

Matrix Matrix::concatenate(Matrix mat) {
    if(rows() != mat.rows()) {
        throw std::runtime_error("Error: concatenating between different rows matrix");
    }
    Matrix copy(*this);
    for(unsigned int i = 0; i < rows(); i++) {
        for(unsigned int j = 0; j < mat.cols(); j++) {
            copy.m_.at(i).push_back( float(mat(i, j)) );
        }
    }
    return copy;
}

}

namespace std {

ekumath::Matrix pow(ekumath::Matrix &mat, const int exp) {
    ekumath::Matrix copy(mat);
    return copy^exp;
}

ekumath::Matrix sqrt(ekumath::Matrix &mat) {
    ekumath::Matrix copy(mat);
    for(unsigned int i = 0; i < copy.rows(); i++) {
        for(unsigned int j = 0; j < copy.cols(); j++) {
            copy(i, j) = sqrt(copy(i, j));
        }
    }    return copy;
}

ostream& operator<<(ostream& os, const ekumath::Matrix& m) {
    ekumath::Matrix mat(m);
    os << "[";
    for(unsigned int i = 0; i < mat.rows(); i++) {
        if(mat.cols() > 1)
            os << "[";
        for(unsigned int j = 0; j < mat.cols(); j++) {
            os << mat(i, j);
            if(j != mat.cols() - 1) {
               os << ", ";
            }
        }
        if(mat.cols() > 1)
            os << "]";
        if(i != mat.rows() - 1) {
            os << ", ";
        }
    }
    os << "]";
    return os;
}

}