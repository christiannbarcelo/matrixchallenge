#ifndef __ekumath__
#define __ekumath__

#include <iostream>
#include <vector>
#include <cstdlib>
#include <math.h> 

namespace ekumath {

class Matrix {

public:
    Matrix() = default;
	Matrix(unsigned int row, unsigned int col, float fill=0);
    Matrix(std::initializer_list<float> init_list);
    Matrix(std::initializer_list<std::initializer_list<float>> init_list);
    ~Matrix() {};
    static Matrix Identity(unsigned int size);
    static Matrix Random(unsigned int row, unsigned int col=0);

    int rows() const;
    int cols() const;
    int size() const;
    Matrix transpose();
    Matrix concatenate(Matrix mat);
    
    bool operator==(const Matrix &other) const;
    bool operator!=(const Matrix &other) const;
    float& operator()(unsigned int row, unsigned int col=0);
    Matrix operator+(Matrix &mat);
    Matrix operator*(Matrix &mat);
    Matrix operator|(Matrix mat);
    Matrix operator^(const int exp);

private:
    std::vector<std::vector<float>> m_;
};

template <class T>
Matrix operator+(const Matrix mat, const T &add) {
    Matrix copy(mat);
    for(unsigned int i = 0; i < copy.rows(); i++) {
        for(unsigned int j = 0; j < copy.cols(); j++) {
            copy(i, j) += add;
        }
    }
    return copy;
}

template <class T>
Matrix operator+(const T &add, const Matrix mat) {
    return mat + add;
}

template <class T>
Matrix operator-(const Matrix mat, const T &sub) {
    return mat + (sub * -1);
}

template <class T>
Matrix operator-(const T &sub, const Matrix mat) {
    return mat - sub;
}

template <class T>
Matrix operator*(const Matrix mat, const T &fact) {
    Matrix copy(mat);
    for(unsigned int i = 0; i < copy.rows(); i++) {
        for(unsigned int j = 0; j < copy.cols(); j++) {
            copy(i, j) *= fact;
        }
    }
    return copy;
}

template <class T>
Matrix operator*(const T &fact, const Matrix mat) {
    return mat * fact;
}

template <class T>
Matrix operator/(const Matrix mat, const T &div) {
    return mat * (1/div);
}

template <class T>
Matrix operator/(const T &div, const Matrix mat) {
    return mat / div;
}

}

namespace std {
ekumath::Matrix pow(ekumath::Matrix &mat, const int exp);
ekumath::Matrix sqrt(ekumath::Matrix &mat);
ostream& operator<<(ostream& os, const ekumath::Matrix& mat);
}

#endif